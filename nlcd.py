"""Constants for the NLCD classification legend."""

FILL                     = 0
WATER                    = 11
SNOW                     = 12
DEVELOPED_OPEN_SPACE     = 21
DEVELOPED_LOW_INTENSITY  = 22
DEVELOPED_MED_INTENSITY  = 23
DEVELOPED_HIGH_INTENSITY = 24
BARREN                   = 31
DECIDUOUS_FOREST         = 41
EVERGREEN_FOREST         = 42
MIXED_FOREST             = 43
SHRUBLAND                = 52
GRASSLAND                = 71
PASTURE                  = 81
CROPLAND                 = 82
WOODY_WETLAND            = 90
HERBACEUOS_WETLAND       = 95

NAMES = {
    WATER : "Water",
    SNOW : "Ice/Snow",
    DEVELOPED_OPEN_SPACE : "Developed, open space",
    DEVELOPED_LOW_INTENSITY : "Developed, low intensity",
    DEVELOPED_MED_INTENSITY : "Developed, med intensity",
    DEVELOPED_HIGH_INTENSITY : "Developed, hi intensity",
    BARREN : "Barren",
    DECIDUOUS_FOREST : "Deciduous forest",
    EVERGREEN_FOREST : "Evergreen forest",
    MIXED_FOREST : "Mixed forest",
    SHRUBLAND : "Shrub/Scrub",
    GRASSLAND : "Grassland",
    PASTURE : "Pasture",
    CROPLAND : "Cropland",
    WOODY_WETLAND : "Woody wetland",
    HERBACEUOS_WETLAND : "Herbaceuos wetland"
}

# NLCD "super-classes"
VEGETATION    = [DECIDUOUS_FOREST,
                EVERGREEN_FOREST,
                MIXED_FOREST,
                SHRUBLAND,
                GRASSLAND,
                PASTURE,
                WOODY_WETLAND,
                HERBACEUOS_WETLAND,
                DEVELOPED_OPEN_SPACE]
URBAN        = [DEVELOPED_LOW_INTENSITY,
                DEVELOPED_MED_INTENSITY,
                DEVELOPED_HIGH_INTENSITY]
WETLAND      = [WOODY_WETLAND, HERBACEUOS_WETLAND]
