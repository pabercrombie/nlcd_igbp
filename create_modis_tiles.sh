#!/bin/bash
#$ -pe omp 8
#$ -l h_rt=50:00:00

######################################################################
# Reproject NLCD map to MODIS Sinusoidal projection, and clip to the
# extent of MODIS tiles. This scripts creates one file for each MODIS
# tile in the continental U.S., containing the NLCD land cover
# classification at 30 m spatial resolution for that tile.
#
# Usage: create_modis_tiles
#
# Author: Parker Abercrombie <parker@pabercrombie.com>
######################################################################

# Path to NLCD classification images.
nlcd_classification=NLCD2006_landcover_4-20-11_se5/nlcd2006_landcover_4-20-11_se5.img
nlcd_tree_canopy=NLCD2001_canopy_mosaic_1-29-08/nlcd_canopy_mosaic_1-29-08.img
nlcd_impervious=NLCD2006_impervious_5-4-11/nlcd2006_impervious_5-4-11.img

#nlcd_classification=NLCD2001_landcover_v2_2-13-11/nlcd2001_landcover_v2_2-13-11.img
#nlcd_tree_canopy=NLCD2001_canopy_mosaic_1-29-08/nlcd_canopy_mosaic_1-29-08.img
#nlcd_impervious=NLCD2001_impervious_v2_5-4-11/nlcd2001_impervious_v2_5-4-11.img

# NLCD no data value
nlcd_nodata=127

# Pixel size of NLCD map
pix_30m=30

# MODIS tiles in the contiguous United States
tiles=(h08v04 h08v05 h08v06 h09v04 h09v05 h09v06 h10v04 h10v05 h10v06 h10v04 h11v04 h11v05 h12v04 h12v05 h13v04)

# MODIS grid parameters
uly_map=10007554.677
ulx_map=-20015109.354
lry_map=-10007554.677
lrx_map=20015109.354
pix_500m=463.312716525
dim_500m=2400

# Proj4 parameters for MODIS Sinusoidal projection.
modis_sinusoidal_proj4="+proj=sinu  +a=6371007.181 +b=6371007.181 +units=m +lon_0=0 +x_0=0 +y_0=0 +no_defs"

# Output size for MODIS tiles at 30m resolution.
dim_30m=$(echo "scale=0; (($pix_500m + $pix_30m - 1) / $pix_30m) * $dim_500m" | bc)

reproject_dir=reprojected
mkdir -p $reproject_dir

nlcd_reprojected=$reproject_dir/$(basename $nlcd_classification .img).bsq
nlcd_canopy_reprojected=$reproject_dir/$(basename $nlcd_tree_canopy .img).bsq
nlcd_impervious_reprojected=$reproject_dir/$(basename $nlcd_impervious .img).bsq

# Reproject full NLCD map to MODIS Sinusoidal projection.
echo Reprojecting NLCD land cover map to MODIS Sinusoidal...

if [ ! -e "$nlcd_reprojected" ]
then
    gdalwarp -of ENVI -co INTERLEAVE=BSQ -ot Byte \
        -t_srs "$modis_sinusoidal_proj4" \
        -tr 30.000000000000000 -30.000000000000000 \
        -r near -overwrite -multi \
        $nlcd_classification $nlcd_reprojected
else
    echo Not reprojecting $nlcd_classification. Reprojected file already exists.
fi

if [ ! -e "$nlcd_canopy_reprojected" ]
then
    gdalwarp -of ENVI -co INTERLEAVE=BSQ -ot Byte \
        -t_srs "$modis_sinusoidal_proj4" \
        -tr $pix_30m $pix_30m \
        -r average -overwrite -multi \
        $nlcd_tree_canopy $nlcd_canopy_reprojected
else
    echo Not reprojecting $nlcd_tree_canopy. Reprojected file already exists.
fi

if [ ! -e "$nlcd_impervious_reprojected" ]
then
    gdalwarp -of ENVI -co INTERLEAVE=BSQ -ot Byte \
        -t_srs "$modis_sinusoidal_proj4" \
        -tr $pix_30m $pix_30m \
        -srcnodata $nlcd_nodata \
        -r average -overwrite -multi \
        $nlcd_impervious $nlcd_impervious_reprojected
else
    echo Not reprojecting $nlcd_impervious. Reprojected file already exists.
fi

# Loop through lines in the tiles file and clip each to the MODIS tile
# extent.
for tile in ${tiles[@]}
do
    # Parse h and v parts out of the tile id.
    if [[ ! $tile =~ h(..)v(..) ]]
    then
        echo Warning: malformed tile id: $tile
        continue
    fi

    tile_h=${BASH_REMATCH[1]}
    tile_v=${BASH_REMATCH[2]}

    lc_out_file_30m=$reproject_dir/${tile}_30m.bsq
    canopy_out_file_30m=$reproject_dir/${tile}_canopy_30m.bsq
    impervious_out_file_30m=$reproject_dir/${tile}_impervious_30m.bsq

    canopy_out_file_500m=$reproject_dir/${tile}_canopy_500m.bsq
    impervious_out_file_500m=$reproject_dir/${tile}_impervious_500m.bsq

    echo Clipping tile $tile

    ulx=$(echo "scale=9; $ulx_map + ($tile_h * $pix_500m * $dim_500m)" | bc)
    uly=$(echo "scale=9; $uly_map - ($tile_v * $pix_500m * $dim_500m)" | bc)
    lrx=$(echo "scale=9; $ulx_map + (($tile_h + 1) * $pix_500m * $dim_500m)" | bc)
    lry=$(echo "scale=9; $uly_map - (($tile_v + 1) * $pix_500m * $dim_500m)" | bc)

    ###############################################
    # Land cover
    ###############################################

    set -x 
    # Clip the land cover raster to the tile boundary. Resize to a
    # multiple of MODIS tile size, so that the result can be easily
    # aggregated to the desired dimension.
    gdal_translate -of ENVI -ot Byte -projwin $ulx $uly $lrx $lry \
        -outsize $dim_30m $dim_30m \
        $nlcd_reprojected $lc_out_file_30m

    ###############################################
    # Tree canopy
    ###############################################

    # Clip the canopy raster to the tile boundary.
    gdal_translate -of ENVI -ot Byte -projwin $ulx $uly $lrx $lry \
        -outsize $dim_30m $dim_30m \
        $nlcd_canopy_reprojected $canopy_out_file_30m

    # Create a version resized to 2400 x 2400. Because the canopy
    # raster values are percentages and not classifications we can let
    # GDAL do the aggregation to MODIS scale.
    gdal_translate -of ENVI -ot Byte -projwin $ulx $uly $lrx $lry \
        -outsize $dim_500m $dim_500m \
        $nlcd_canopy_reprojected $canopy_out_file_500m

    ###############################################
    # Impervious surface
    ###############################################

    # Clip the impervious surface raster to the tile boundary.
    gdal_translate -of ENVI -ot Byte -projwin $ulx $uly $lrx $lry \
        -outsize $dim_30m $dim_30m \
        $nlcd_impervious_reprojected $impervious_out_file_30m

    # Resize tile to 2400 x 2400. As in the canopy case, we let GDAL
    # aggregate the percentage values to MODIS scale.
    gdal_translate -of ENVI -ot Byte -projwin $ulx $uly $lrx $lry \
        -outsize $dim_500m $dim_500m \
        $nlcd_impervious_reprojected $impervious_out_file_500m
done

echo Reprojection complete.
