#!/bin/bash

echo Downloading NLCD 2006 (this will take a while)
wget http://tdds.cr.usgs.gov/landcover/NLCD2006/NLCD2006_landcover_4-20-11_se5.zip
echo Unzipping NLCD 2006....
unzip NLCD2006_landcover_4-20-11_se5.zip
