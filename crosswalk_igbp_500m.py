#!/bin/env python

"""Usage: crosswalk_igbp_500m tile_file"""

import sys
import itertools

import gdal
from gdalconst import *

import numpy as np
from numpy.lib.stride_tricks import as_strided

import igbp
import nlcd

VERSION = "1.2"

LC_PATTERN         = "reprojected/{TILE}_30m.bsq"
CANOPY_PATTERN     = "reprojected/{TILE}_canopy_500m.bsq"
IMPERVIOUS_PATTERN = "reprojected/{TILE}_impervious_500m.bsq"
OUTPUT_PATTERN     = "igbp_500m_v{VERSION}/{TILE}.bsq"

def crosswalk_nlcd_igbp(percent_tree, percent_impervious, nlcd_px):
    # Determine if pixel is mostly vegetated, or mostly not vegetated.
    percent_veg = np.sum(nlcd_px[nlcd.VEGETATION])
    if percent_veg >= 0.5:
        # If the pixel is more than 30% water, or more than 60 water
        # and wetland, call it a wetland
        if (nlcd_px[nlcd.WATER] >= 0.3 or
            nlcd_px[nlcd.WATER] + np.sum(nlcd_px[nlcd.WETLAND]) >= 0.6):
            return igbp.WETLAND

        # If the pixel is more than 60% tree covered, call it a
        # forest. Use the NLCD classification to determine
        # evergreen/deciduous/mixed. Note that we cannot separate
        # needleleaf from broadleaf, so just call everything IGBP
        # Needleleaf.
        if percent_tree >= 0.6:
            if nlcd_px[nlcd.DECIDUOUS_FOREST] >= 0.6:
                return igbp.DECIDUOUS_BROADLEAF_FOREST
            elif nlcd_px[nlcd.EVERGREEN_FOREST] >= 0.6:
                return igbp.EVERGREEN_NEEDLELEAF_FOREST
            else:
                return igbp.MIXED_FOREST

        percent_ag = nlcd_px[nlcd.CROPLAND]
        if percent_ag >= 0.3:
            return igbp.CROPLAND_MOSAIC

        elif percent_tree >= 0.3:
            return igbp.WOODY_SAVANNA
        elif percent_tree >= 0.1:
            return igbp.SAVANNA

        # If tree cover is less than 10% then it's either a shrubland
        # or grassland. Label according to max NLCD class. Note that
        # we cannot seperate closed and open shrublands, so call
        # everything open.
        if nlcd_px[nlcd.SHRUBLAND] >= nlcd_px[nlcd.GRASSLAND] + nlcd_px[nlcd.PASTURE]:
            return igbp.OPEN_SHRUBLAND
        else:
            return igbp.GRASSLAND

    else: # Non-vegetated

        # If 30% of the pixel is impervious, or more than 50% is
        # classified as urban in NLCD, call it urban.
        if (percent_impervious >= 0.3 or
            np.sum(nlcd_px[nlcd.URBAN]) >= 0.5):
            return igbp.URBAN

        # If at least 30% is agriculture, then call it agriculture or
        # ag mosaic.
        percent_ag = nlcd_px[nlcd.CROPLAND]
        if percent_ag >= 0.6:
            return igbp.CROPLAND
        elif percent_ag >= 0.3:
            return igbp.CROPLAND_MOSAIC

        # Pixel must be either barren, snow, or water. Classifify as
        # max scoring class from NLCD.
        max_label = nlcd_px.argmax()
        if max_label == nlcd.BARREN:
            return igbp.BARREN
        elif max_label == nlcd.SNOW:
            return igbp.SNOW

        return igbp.WATER

    assert "Warning: unclassified pixel", nlcd

def aggregation_igbp(pixels):
    """Aggregate many high resolution pixels to one low resolution pixel.

    Arguments:
       pixels - NumPy array of pixel values. The first value in the
                array is the average percent tree cover of all these
                high resolution pixels. The second value is the
                average percent impervious surface. Remaining values
                are NLCD land cover classification values.
    Return:
       Low resolution IGBP label for these high resolution pixels.
    """

    # Count how many times each NLCD label appears in this set of
    # pixels. bincount returns a vector of length 96, where index 1
    # contains the count of 1, index 2 contains count of 2, tc. 
    hist = np.bincount(pixels[2:], minlength=96).astype(float)

    # Noramalize the histogram. Need to check for zero because the
    # window could be all no data values.
    tot = np.sum(hist[1:])
    if tot != 0:
        hist = hist / tot
        igbp = crosswalk_nlcd_igbp(pixels[0] / 100.0,
                                   pixels[1] / 100.0,
                                   hist)
        return igbp
    else: # All fill, just return fill value
        return 0

def aggregate (g, canopy_raster, impervious_raster, fname_out, out_x, out_y):
    lc_pixels = g.GetRasterBand(1).ReadAsArray()
    canopy_pixels = canopy_raster.GetRasterBand(1).ReadAsArray()
    impervious_pixels = impervious_raster.GetRasterBand(1).ReadAsArray()

    scale_x = lc_pixels.shape[1] / out_x
    scale_y = lc_pixels.shape[0] / out_y

    geoT = g.GetGeoTransform()

    src_pix_x = geoT[1]
    src_pix_y = geoT[5]
    dst_pix_x = src_pix_x * scale_x
    dst_pix_y = src_pix_y * scale_y

    # Create a view of the data as a 4D array, in which the first two
    # dimension are the dimensions of the lo-res image (ie 2400 x
    # 2400), and within each of these cells is a 2D array that
    # contains the hi-res pixels contained in the lo-res pixel.
    b = as_strided(lc_pixels,
                   shape=(lc_pixels.shape[0] / scale_x,
                          lc_pixels.shape[1] / scale_y, scale_x, scale_y),
                   strides=(scale_x * lc_pixels.strides[0],
                            scale_y * lc_pixels.strides[1],
                            lc_pixels.strides[0],
                            lc_pixels.strides[1]))

    # Reshape the array into a 3D array in which the first two
    # dimensions are the dimensions of the lo-res image, and the third
    # dimension is a vector of the hi-res pixels that correspond to
    # the lo-res pixel.
    b = b.reshape((b.shape[0], b.shape[1], -1))

    # Concatenate the canopy %, impervious %, and classification
    # pixels. The resulting array has three dimensions. The third
    # dimension is a vector [% tree, % impervious, hi-res pixels...]
    b = np.dstack((canopy_pixels, impervious_pixels, b))

    # Apply the aggregation function on the 3rd axis. This calls the
    # aggregation function for each lo-res pixel, passing a vector of
    # hi-res pixels that fall within the lo-res pixel. The result is a
    # 2D array. 
    result = np.apply_along_axis(aggregation_igbp, 2, b)

    # Package the lo-res data array into a raster dataset.
    drv = gdal.GetDriverByName("ENVI")
    resampled = drv.Create(fname_out, out_x, out_y, 1, gdal.GDT_Byte)

    # Apply the same georeference, with a different spatial resolution.
    this_geoT = (geoT[0], geoT[1] * scale_x, geoT[2], geoT[3], geoT[4], geoT[5] * scale_y)
    resampled.SetGeoTransform(this_geoT)
    resampled.SetProjection(g.GetProjectionRef())

    resampled.GetRasterBand(1).WriteArray(result)

    resampled = None
    result = None
    lc_pixels = None
    canopy_pixels = None
    impervious_pixels = None

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print __doc__
        exit(1)

    with open(sys.argv[1]) as f:
        for tile in f:
            tile = tile.strip()
            if tile[0] == '#':
                continue # Skip comments

            print "Processing {}".format(tile)        

            filename = LC_PATTERN.format(TILE=tile)
            lc_dataset = gdal.Open(filename, GA_ReadOnly)
            if lc_dataset is None:
                print "Error: can't read dataset {}".format(filename)
                exit(1)

            filename = CANOPY_PATTERN.format(TILE=tile)
            canopy_dataset = gdal.Open(filename, GA_ReadOnly)
            if canopy_dataset is None:
                print "Error: can't read dataset {}".format(filename)
                exit(1)

            filename = IMPERVIOUS_PATTERN.format(TILE=tile)        
            impervious_dataset = gdal.Open(filename, GA_ReadOnly)
            if canopy_dataset is None:
                print "Error: can't read dataset {}".format(filename)
                exit(1)

            output = OUTPUT_PATTERN.format(TILE=tile, VERSION=VERSION)

            aggregate(lc_dataset, canopy_dataset, impervious_dataset, output, 2400, 2400)
