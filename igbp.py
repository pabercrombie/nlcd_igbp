"""Constants for the IGBP classification legend."""

EVERGREEN_NEEDLELEAF_FOREST = 1
EVERGREEN_BROADLEAF_FOREST  = 2
DECIDUOUS_NEEDLELEAF_FOREST = 3
DECIDUOUS_BROADLEAF_FOREST  = 4
MIXED_FOREST                = 5
CLOSED_SHRUBLAND            = 6
OPEN_SHRUBLAND              = 7
WOODY_SAVANNA               = 8
SAVANNA                     = 9
GRASSLAND                   = 10
WETLAND                     = 11
CROPLAND                    = 12
URBAN                       = 13
CROPLAND_MOSAIC             = 14
SNOW                        = 15
BARREN                      = 16
WATER                       = 17
