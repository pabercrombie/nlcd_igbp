#! /bin/bash
#$ -pe omp 1
#$ -l h_rt=48:00:00

###############################################################################
# Create mosaics from decision tree output maps. Copy desired map tiles to
# current directory before running this script.
###############################################################################

# last modified 7/26/13 by Parker Abercrombie

in_files=igbp_500m_v1.2/*.bsq

outdir=igbp_500m_v1.2
mkdir -p $outdir

temp_dir=$(mktemp --tmpdir=. -d)

# Approximate upper left and lower right corners of the lower 48 states in Albers
# Conical Equal Area
conus_ulx=-2539885
conus_uly=3348070
conus_lrx=2336890
conus_lry=71663

modis_sinusoidal_proj4='+proj=sinu +a=6371007.181 +b=6371007.181 +units=m'
albers_conical_eq_area_proj4='+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-96 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'

# Merge the data into one big mosaic
echo
echo Merging tiles into one file...
gdal_merge.py -of ENVI -o $temp_dir/merged.bsq $in_files

# Reproject to lat long
echo
echo Reprojecting to Albers Conical Equal Area...
gdalwarp -s_srs "$modis_sinusoidal_proj4" -t_srs "$albers_conical_eq_area_proj4" -r near -overwrite -of ENVI $temp_dir/merged.bsq $temp_dir/warped.bsq

# Add class labels to the header file
cat ../thesis/src/envi/igbp.hdr >> $temp_dir/warped.hdr

echo
echo Generating PNG file...
gdal_translate -of PNG -outsize 50% 50% -projwin $conus_ulx $conus_uly $conus_lrx $conus_lry $temp_dir/warped.bsq $outdir/nlcd_igbp.png

rm $temp_dir/*
rmdir $temp_dir
