#!/bin/env python

"""Usage: aggregate files"""

import sys
import itertools

import gdal
from gdalconst import *

import numpy as np
from numpy.lib.stride_tricks import as_strided

NLCD_WATER                    = 11
NLCD_SNOW                     = 12
NLCD_DEVELOPED_OPEN_SPACE     = 21
NLCD_DEVELOPED_LOW_INTENSITY  = 22
NLCD_DEVELOPED_MED_INTENSITY  = 23
NLCD_DEVELOPED_HIGH_INTENSITY = 24
NLCD_BARREN                   = 31
NLCD_DECIDUOUS_FOREST         = 41
NLCD_EVERGREEN_FOREST         = 42
NLCD_MIXED_FOREST             = 43
NLCD_SHRUBLAND                = 52
NLCD_GRASSLAND                = 71
NLCD_PASTURE                  = 81
NLCD_CROPLAND                 = 82
NLCD_WOODY_WETLAND            = 90
NLCD_HERBACEUOS_WETLAND       = 95

NLCD_NAMES = {
    NLCD_WATER : "Water",
    NLCD_SNOW : "Ice/Snow",
    NLCD_DEVELOPED_OPEN_SPACE : "Developed, open space",
    NLCD_DEVELOPED_LOW_INTENSITY : "Developed, low intensity",
    NLCD_DEVELOPED_MED_INTENSITY : "Developed, med intensity",
    NLCD_DEVELOPED_HIGH_INTENSITY : "Developed, hi intensity",
    NLCD_BARREN : "Barren",
    NLCD_DECIDUOUS_FOREST : "Deciduous forest",
    NLCD_EVERGREEN_FOREST : "Evergreen forest",
    NLCD_MIXED_FOREST : "Mixed forest",
    NLCD_SHRUBLAND : "Shrub/Scrub",
    NLCD_GRASSLAND : "Grassland",
    NLCD_PASTURE : "Pasture",
    NLCD_CROPLAND : "Cropland",
    NLCD_WOODY_WETLAND : "Woody wetland",
    NLCD_HERBACEUOS_WETLAND : "Herbaceuos wetland"
}

NLCD_VEG = [NLCD_DECIDUOUS_FOREST,
            NLCD_EVERGREEN_FOREST,
            NLCD_MIXED_FOREST,
            NLCD_SHRUBLAND,
            NLCD_GRASSLAND,
            NLCD_WOODY_WETLAND,
            NLCD_HERBACEUOS_WETLAND]

NLCD_URBAN = [NLCD_DEVELOPED_OPEN_SPACE,
              NLCD_DEVELOPED_LOW_INTENSITY,
              NLCD_DEVELOPED_MED_INTENSITY,
              NLCD_DEVELOPED_HIGH_INTENSITY]

NLCD_WETLAND = [NLCD_WOODY_WETLAND,
                NLCD_HERBACEUOS_WETLAND,
                NLCD_WATER]

NLCD_AGRICULTURE = [NLCD_CROPLAND,
                    NLCD_PASTURE]

IGBP_EVERGREEN_NEEDLELEAF_FOREST = 1
IGBP_EVERGREEN_BROADLEAF_FOREST  = 2
IGBP_DECIDUOUS_NEEDLELEAF_FOREST = 3
IGBP_DECIDUOUS_BROADLEAF_FOREST  = 4
IGBP_MIXED_FOREST                = 5
IGBP_CLOSED_SHRUBLAND            = 6
IGBP_OPEN_SHRUBLAND              = 7
IGBP_WOODY_SAVANNA               = 8
IGBP_SAVANNA                     = 9
IGBP_GRASSLAND                   = 10
IGBP_WETLAND                     = 11
IGBP_CROPLAND                    = 12
IGBP_URBAN                       = 13
IGBP_CROPLAND_MOSAIC             = 14
IGBP_SNOW                        = 15
IGBP_BARREN                      = 16
IGBP_WATER                       = 17

def crosswalk_nlcd_igbp(percent_tree, percent_impervious, nlcd):
    # Determine if pixel is mostly vegetated, or mostly not vegetated.
    percent_veg = np.sum(nlcd[NLCD_VEG])
    if percent_veg >= 0.5:
        # If the pixel is more than 30% water wetland, call it a
        # wetland
        if np.sum(nlcd[NLCD_WETLAND]) >= 0.3:
            return IGBP_WETLAND

        # If the pixel is more than 60% tree covered, call it a
        # forest. Use the NLCD classification to determine
        # evergreen/deciduous/mixed. Note that we cannot separate
        # needleleaf from broadleaf, so just call everything IGBP
        # Needleleaf.
        if percent_tree >= 0.6:
            if nlcd[NLCD_DECIDUOUS_FOREST] >= 0.6:
                return IGBP_DECIDUOUS_BROADLEAF_FOREST
            elif nlcd[NLCD_EVERGREEN_FOREST] >= 0.6:
                return IGBP_EVERGREEN_NEEDLELEAF_FOREST
            else:
                return IGBP_MIXED_FOREST

        elif percent_tree >= 0.3:
            return IGBP_WOODY_SAVANNA
        elif percent_tree >= 0.1:
            return IGBP_SAVANNA

        # If tree cover is less than 10% then it's either a shrubland
        # or grassland. Label according to max NLCD class. Note that
        # we cannot seperate closed and open shrublands, so call
        # everything open.
        if nlcd[NLCD_SHRUBLAND] >= nlcd[NLCD_GRASSLAND]:
            return IGBP_OPEN_SHRUBLAND
        else:
            return IGBP_GRASSLAND

    else: # Non-vegetated

        # If 30% of the pixel is impervious, or more than 50% is
        # classified as urban in NLCD, call it urban.
        if (percent_impervious >= 0.3 or
            np.sum(nlcd[NLCD_URBAN]) >= 0.5):
            return IGBP_URBAN

        # If at least 30% is agriculture, then call it agriculture or
        # ag mosaic.
        percent_ag = np.sum(nlcd[NLCD_AGRICULTURE])
        if percent_ag >= 0.6:
            return IGBP_CROPLAND
        elif percent_ag >= 0.3:
            return IGBP_CROPLAND_MOSAIC

        # Pixel must be either barren, snow, or water. Classifify as
        # max scoring class from NLCD.
        max_label = nlcd.argmax()
        if max_label == NLCD_BARREN:
            return IGBP_BARREN
        elif max_label == NLCD_SNOW:
            return IGBP_SNOW

        return IGBP_WATER

    assert "Warning: unclassified pixel", nlcd

def aggregation_igbp(pixels):
    # Count how many times each NLCD label appears in this set of
    # pixels. bincount returns a vector of length 96, where index 1
    # contains the count of 1, index 2 contains count of 2, tc. 
    hist = np.bincount(pixels[2:], minlength=96).astype(float)

    # Noramalize the histogram. Need to check for zero because the
    # window could be all no data values.
    tot = np.sum(hist[1:])
    if tot != 0:
        hist = hist / tot
        igbp = crosswalk_nlcd_igbp(pixels[0] / 100.0,
                                   pixels[1] / 100.0,
                                   hist)
        return igbp
    else: # All fill, just return fill value
        return 0

def aggregate (g, canopy_raster, impervious_raster, fname_out, out_x, out_y):
    lc_pixels = g.GetRasterBand(1).ReadAsArray()
    canopy_pixels = canopy_raster.GetRasterBand(1).ReadAsArray()
    impervious_pixels = impervious_raster.GetRasterBand(1).ReadAsArray()

    scale_x = lc_pixels.shape[1] / out_x
    scale_y = lc_pixels.shape[0] / out_y

    geoT = g.GetGeoTransform()

    src_pix_x = geoT[1]
    src_pix_y = geoT[5]
    dst_pix_x = src_pix_x * scale_x
    dst_pix_y = src_pix_y * scale_y

    # Create a view of the data as a 4D array, in which the first two
    # dimension are the dimensions of the lo-res image (ie 2400 x
    # 2400), and within each of these cells is a 2D array that
    # contains the hi-res pixels contained in the lo-res pixel.
    b = as_strided(lc_pixels,
                   shape=(lc_pixels.shape[0] / scale_x,
                          lc_pixels.shape[1] / scale_y, scale_x, scale_y),
                   strides=(scale_x * lc_pixels.strides[0],
                            scale_y * lc_pixels.strides[1],
                            lc_pixels.strides[0],
                            lc_pixels.strides[1]))

    # Reshape the array into a 3D array in which the first two
    # dimensions are the dimensions of the lo-res image, and the third
    # dimension is a vector of the hi-res pixels that correspond to
    # the lo-res pixel.
    b = b.reshape((b.shape[0], b.shape[1], -1))

    # Concatenate the canopy %, impervious %, and classification
    # pixels. The resulting array has three dimensions. The third
    # dimension is a vector [% tree, % impervious, hi-res pixels...]
    b = np.dstack((canopy_pixels, impervious_pixels, b))

    # Apply the aggregation function on the 3rd axis. This calls the
    # aggregation function for each lo-res pixel, passing a vector of
    # hi-res pixels that fall within the lo-res pixel. The result is a
    # 2D array. 
    result = np.apply_along_axis(aggregation_igbp, 2, b)

    # Package the lo-res data array into a raster dataset.
    drv = gdal.GetDriverByName("ENVI")
    resampled = drv.Create(fname_out, out_x, out_y, 1, gdal.GDT_Byte)

    # Apply the same georeference, with a different spatial resolution.
    this_geoT = (geoT[0], geoT[1] * scale_x, geoT[2], geoT[3], geoT[4], geoT[5] * scale_y)
    resampled.SetGeoTransform(this_geoT)
    resampled.SetProjection(g.GetProjectionRef())

    resampled.GetRasterBand(1).WriteArray(result)

    resampled = None
    result = None
    lc_pixels = None
    canopy_pixels = None
    impervious_pixels = None

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print __doc__
        exit(1)

    for f in sys.argv[1:]:
        print "Processing {}".format(f)

        lc_dataset = gdal.Open("reprojected/{}.bsq".format(f), GA_ReadOnly)
        if lc_dataset is None:
            print "Error: can't read dataset {}".format(filename)
            exit(1)

        canopy_dataset = gdal.Open("reprojected/{}_canopy.bsq".format(f), GA_ReadOnly)
        if canopy_dataset is None:
            print "Error: can't read dataset {}".format(filename)
            exit(1)

        impervious_dataset = gdal.Open("reprojected/{}_impervious.bsq".format(f), GA_ReadOnly)
        if canopy_dataset is None:
            print "Error: can't read dataset {}".format(filename)
            exit(1)

        output = "igbp/{}.bsq".format(f)

        aggregate(lc_dataset, canopy_dataset, impervious_dataset, output, 2400, 2400)
